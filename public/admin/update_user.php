<?php 
//ob_start();

require ("../../private/initialize.php");
?>
<div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Update User</h1>
            
        </div>
    </div>
    <div class="col-lg-8">
        <div class="form-group">
            <form action="update_user.php" method="POST" autocomplete="off">
                <input type="hidden" autocomplete="off"> 
                <label for="find_user">Search for User by Username:</label><br>
                <input type="text" id="sr_username" name="username" class="form-control" value="">
                <div id="search_result">
                    <div id="result" class="col-lg-4">
                    </div>
                </div><br>
                <input type="submit" name="submit" value="Search" class="btn btn-primary">
            </form>
        </div>
        <?php

        if(isset($_POST['submit'])){
            Mapper::set_database();
            if(Mapper::checkUser($_POST) === false){
                echo "No such user";
                exit;
            } else {
                $users = Mapper::showUser($_POST);
            }
            ?>
            <form action="update_user.php" method="POST" id="update_user">
                <table class="table">
                    <tr>
                        <th style="width:400px;">UserID</th>
                        <td><input type="text" name="users_id" id="users_id" value="<?php echo $users['users_id']; ?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <th>Username</th>
                        <td><input type="text" name="username" id="username" value="<?php echo $users['username']; ?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>
                            <input type="text" name="status" id="status" value="<?php echo $users['status']; ?>" class="form-control">
                            <input type="hidden" name="is_status" id="is_status" value="">
                        </td>
                    </tr>                   
                    <tr id="st_JMBG">
                        <th>Students JMBG</th>
                        <td><input type="text" name="student_JMBG" id="student_JMBG" value="<?php if(isset($users['student_JMBG'])){echo $users['student_JMBG'];} else {$users['student_JMBG'] = '';} ?>" class="form-control"></td>
                     </tr>                              
                    <tr id="st_group_year">
                        <th>Students Group Year</th>
                        <td><input type="text" name="group_year" id="student_group_year" value="<?php if(isset($users['group_year'])){echo $users['group_year'];} else {$users['group_year'] = '';} ?>" class="form-control"></td>
                    </tr>
                    <tr id="st_group_number">
                        <th>Students Group Number</th>
                        <td><input type="text" name="group_number" id="student_group_number" value="<?php if(isset($users['group_number'])){echo $users['group_number'];} else {$users['group_number'] = '';} ?>" class="form-control"></td>
                    </tr>                               
                    <tr>
                        <th>Name</th>
                        <td><input type="text" name="firstName" id="firstName" value="<?php echo $users['firstName']; ?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <th>Lastname</th>
                        <td><input type="text" name="lastName" id="lastName" value="<?php echo $users['lastName']; ?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="upd_submit" value="Save Changes" class="btn btn-primary"></td>
                    </tr>
                </table>
            </form>
            <?php
        }

        if(isset($_POST['upd_submit'])){
            Mapper::set_database();
            if(Mapper::updateUser($_POST)){
                echo "<p>Data saved.</p>";
            }
            
        }
        ?>

</div>
<!-- /.container-fluid -->

</div>
<script>

    $(document).ready(function(){

        var status = $('#status').val();
        var is_status = $('#is_status').val(status);
        var username = $('#username').val();
        
        if(status == 4){
            $('#st_JMBG').show(500);
            $('#student_group_year').val("");
            $('#student_group_number').val("");
        } else {
            $('#st_JMBG').hide();
        }

        if(status == 3){
            $('#st_group_year').show(500);
            $('#st_group_number').show(500);
            $('#student_JMBG').val("");
        } else {
            $('#st_group_year').hide(100);
            $('#st_group_number').hide(100);
        }

        if(status == 2){
            $('#st_JMBG').hide(100);
            $('#st_group_year').hide(100);
            $('#st_group_number').hide(100);
            $('#student_group_year').val("");
            $('#student_group_number').val("");
            $('#student_JMBG').val("");
        }
        $('#result').hide();     
        $('#sr_username').keyup(function(){
            $('#result').show();
            var username = $('#sr_username').val();
            $.post("find_user_by_username.php", {
                inputVal : username
            }, function(data, status){
                $('#result').html(data);            
            });
        });

        $(document).on('click', 'p.user', function(){
            var text = $.text(this);
            $('#sr_username').val(text);
            $('#result').hide();            
        });

        function styleOnTrue(obj){
            $(obj).css({
                "background-color" : "#FFDACD",
                "border-color" : "#FF9C83"
            });
        }

        function styleOnFalse(obj){
            $(obj).css({
                "background-color" : "#FFFFFF",
                "border-color" : "#CCCCCC"
            });
        }

        var users_id = $('#users_id').val();
        $(document).on('input', '#users_id', function(){
            var user_id = $('#users_id').val();
            if(user_id != users_id){
                styleOnTrue($('#users_id'));
                $(this).attr('placeholder', 'Cannot change User ID.');                
            } else {
                styleOnFalse($('#users_id'));
            }
        });

        $(document).on('focusout', '#users_id', function(){
            $(this).val(users_id);
            styleOnFalse($('#users_id'));
        });

        $(document).on('input', '#username', function(){
            var username = $('#username').val();
            if(username.length < 2){
                styleOnTrue($('#username'));
                $(this).attr('placeholder', 'Must be at least 2 characters long.');
            } else {
                styleOnFalse($('#username'));
            }
        });

        $(document).on('focusout', '#status', function(){
            var new_status = $('#status').val();
            if(status != new_status){
                var conf = confirm("You are about to change status for user: " + username + "\nAre you sure?");
                if(conf){
                    if(new_status == 2){
                        $('#st_group_year').hide(600);
                        $('#st_group_number').hide(600);
                        $('#st_JMBG').hide(600);
                    }

                    if(new_status == 3){
                        $('#st_group_year').show(800);
                        $('#st_group_number').show(800);
                        $('#st_JMBG').hide();
                    }

                    if(new_status == 4){
                        $('#st_JMBG').show(800);
                        $('#st_group_number').hide();
                        $('#st_group_year').hide();
                    }
                }
                status = new_status;
                return status;
            }
        });
        
        $(document).on('input', '#student_JMBG', function(){
            var student_JMBG = $('#student_JMBG').val();            
            var reg = new RegExp('^[1-3][0-9]{12}$');
            if(!reg.test(student_JMBG)){
                    styleOnTrue($('#student_JMBG'));
                    $(this).attr("placeholder","Must be number and 13 characters long.");                        
            } else {
                styleOnFalse($('#student_JMBG'));
            }
        });

        $(document).on('input', '#student_group_year', function(){
            var student_group_year = $(this).val();
            var reg = new RegExp('^[1-8]$');
            if(!reg.test(student_group_year)){
                styleOnTrue($('#student_group_year'));
                $(this).attr("placeholder", "Between 1-8");
            } else {
                styleOnFalse($('#student_group_year'));
            }
        });

        $(document).on('input', '#student_group_number', function(){
            var student_group_number = $(this).val();
            var reg = new RegExp('^[1-8]$');
            if(!reg.test(student_group_number)){
                styleOnTrue($('#student_group_number'));
                $(this).attr("placeholder", "Between 1-8");
            } else {
                styleOnFalse($('#student_group_number'));
            }
        });

        $(document).on('input', '#status', function(){
            var status = $(this).val();
            var reg = new RegExp('^[1-4]$');
            if(!reg.test(status)){
                styleOnTrue($('#status'));
                $(this).attr("placeholder","Acceptable values: 2, 3 or 4.");
            } else {
                styleOnFalse($('#status'));
            }
        });

        $(document).on('input', '#firstName', function(){
            var firstName = $(this).val();
            if(firstName.length < 2){
                styleOnTrue($('#firstName'));
                $(this).attr("placeholder", "Must be at least two characters long.");
            } else {
                styleOnFalse($('#firstName'));
            }
        });

        $(document).on('input', '#lastName', function(){
            var lastName = $(this).val();
            if(lastName.length < 2){
                styleOnTrue($('#lastName'));
                $(this).attr("placeholder", "Must be at least two characters long.");
            } else {
                styleOnFalse($('#lastName'));
            }
        });
        
        $('#update_user').submit(function(e){
            
            var err = [];
            var student_JMBG_reg = new RegExp('^[1-3][0-9]{12}$');
            var student_group_reg = new RegExp('^[1-8]$');
            var status_reg = new RegExp('^[1-4]$');
            var user_id = $('#users_id').val(); 
            
            if(user_id != users_id){
                err.push($('#users_id').attr('id'));
            }

            if($('#username').val().length < 2){
                err.push($('#username').attr('id'));            
            }

            if(!status_reg.test(status)){
                err.push($('#status').attr('id')); 
            }
            
            if(!student_JMBG_reg.test($('#student_JMBG').val()) && $('#student_group_year').val() == "" && $('#student_group_number').val() == ""){
                if($('#status').val() != 2){
                    err.push($('#student_JMBG').attr('id'));
                } else {
                    err.push();
                } 
            }

            if(!student_group_reg.test($('#student_group_year').val()) && $('#student_JMBG').val() == ""){
                err.push($('#student_group_year').attr('id')); 
            }

            if(!student_group_reg.test($('#student_group_number').val()) && $('#student_JMBG').val() == ""){
                err.push($('#student_group_number').attr('id')); 
            }

            if($('#firstName').val().length < 2){
                err.push($('#firstName').attr('id'));
            }

            if($('#lastName').val().length < 2){
                err.push($('#lastName').attr('id'));
            }
            
            if(err.length == 0){
                e.submit();
            } else {                
                e.preventDefault();
            }
          

        });
    });

</script>
<?php include("../../private/styles/includes/footer.php"); ?>

